$(document).ready(function() {

// OWL CAROUSEL ===============================================
  $(".owl-carousel-0").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
  $(".grano-carousel-conteudo").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
    $(".grano-carousel-conteudo-1").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,
      nav: true

  });
// /OWL CAROUSEL ==============================================

$(document).ready(function(){
  $(".slider-active").owlCarousel({
  loop:true,
  nav:true,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
  });
});


// bxslider crousel ===========================================
  var owlHome = $('.owl-carousel');
  owlHome.owlCarousel({
    items:1,
    dots: false,
    loop:true,
    autoplay:true,
    autoplayHoverPause:true
  });


// Back to Top ================================================
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');
// /Back to Top ===============================================


// Grano Scroll ANIMATION =====================================
obj = {
  '.textocombotao-1 .row .texto' : 'fadeIn',
  '.textocombotao-2 .row .texto' : 'fadeIn',
  '.textocombotao-3 .row .texto' : 'fadeInUp'
 }

GranoScrollAni(obj);

// / Grano Scroll ANIMATION ===================================

// / Banner home portfolio div3 ===============================


$(document).ready(function () {
        if($(document).width() >= 992){
          $('#Carousel').carousel({
              interval: 5000
          });
      }
});





  
// / Banner home portfolio div3 ===============================



// / saiba mais div4 ==========================================

  $("#saiba-mais1").click(function(){
    $("#mask-saibamais1").css({"height": "100vh","opacity": ".9"});
    $("#mask-height1").css({"height": "auto"});
  });


  $("#saiba-mais2").click(function(){
    $("#mask-saibamais2").css({"height": "100vh","opacity": ".9"});
    $("#mask-height2").css({"height": "auto"});
  });

  $("#sair1").click(function(){
    $("#mask-saibamais1").css({"height": "0vh","opacity": "0"});
    $("#mask-height1").css({"height": "0"});
  });  

  $("#sair2").click(function(){
    $("#mask-saibamais2").css({"height": "0vh","opacity": "0"});
    $("#mask-height2").css({"height": "0"});
  });  



// / saiba mais div4 ==========================================


// / toogle list div1 =========================================
$(document).ready(function () {
  
  $('#toggle-view li').click(function () {

    var text = $(this).children('div.panel');

    if (text.is(':hidden')) {
      text.slideDown('200');
      $(this).children('span').html('-');   
    } else {
      text.slideUp('200');
      $(this).children('span').html('+');   
    }
    
  });

});
// / toogle list div1 =========================================

 


$(document).ready(function () {
        if($(document).width() >= 992){
          (function($) {                            
              $(window).scroll(function(){                          
                  if ($(this).scrollTop() > 250) {
                      $('.navbar').css({"background-color": "rgba(255,255,255,.8)","border-bottom": "1px solid rgba(211, 112, 76, .5)"});
                      $('.navbar-brand img').css({"max-width": "120px","margin-top":"-5px"});
                  } else {
                      $('.navbar').css({"background": "none","border": "none"});
                      $('.navbar-brand img').css({"max-width": "170px","margin-top":"10px"});
                  }
              });
        })(jQuery);
      }
});

    $('#menu-item-6').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div1').offset().top -50
        }, 500);
      });
    $('#menu-item-8').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div3').offset().top -50
        }, 500);
      }); 
    $('#menu-item-9').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div4').offset().top -50
        }, 500);
      });   
    $('#menu-item-10').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div5').offset().top -50
        }, 500);
      });  
    $('.botao-banner').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div1').offset().top -50
        }, 500);
      });
    $('.botao-nossos-servicos').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div2').offset().top -50
        }, 500);
      });            
    $('.botao-nossos-servicos').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div2').offset().top -50
        }, 500);
      });  
    $('#menu-item-68').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div2').offset().top -50
        }, 500);
      });                                
    $('#menu-item-69').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div2').offset().top -50
        }, 500);
      });
    $('#menu-item-70').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div2').offset().top -50
        }, 500);
      });  
    $('#menu-item-71').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div2').offset().top -50
        }, 500);
      });
    $('#menu-item-72').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div2').offset().top -50
        }, 500);
      });          
    $('#menu-item-73').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div2').offset().top -50
        }, 500);
      });  
    $('#menu-item-74').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div2').offset().top -50
        }, 500);
      });              
});