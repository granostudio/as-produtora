<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

 //======================== Custom post type para serviços ========================// 

 function custom_post_type() {

 // Set UI labels for Custom Post Type
 	$labels = array(
 		'name'                => _x( 'Serviços', 'Post Type General Name', 'twentythirteen' ),
 		'singular_name'       => _x( 'Serviço', 'Post Type Singular Name', 'twentythirteen' ),
 		'menu_name'           => __( 'Serviços', 'twentythirteen' ),
 		'parent_item_colon'   => __( 'Parent Serviço', 'twentythirteen' ),
 		'all_items'           => __( 'Todos os Serviços', 'twentythirteen' ),
 		'view_item'           => __( 'View Movie', 'twentythirteen' ),
 		'add_new_item'        => __( 'Adicionar novo serviço', 'twentythirteen' ),
 		'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
 		'edit_item'           => __( 'Editar Serviço', 'twentythirteen' ),
 		'update_item'         => __( 'Update Serviço', 'twentythirteen' ),
 		'search_items'        => __( 'Procurar Serviço', 'twentythirteen' ),
 		'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
 		'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
 	);

 // Set other options for Custom Post Type

 	$args = array(
 		'label'               => __( 'servicos', 'twentythirteen' ),
 		'description'         => __( 'Movie news and reviews', 'twentythirteen' ),
 		'labels'              => $labels,
 		// Features this CPT supports in Post Editor
 		'supports'            => array( 'title', 'editor', 'thumbnail' ),
 		// You can associate this CPT with a taxonomy or custom taxonomy.
 	// 	'taxonomies'          => array( 'genres' ),
 		/* A hierarchical CPT is like Pages and can have
 		* Parent and child items. A non-hierarchical CPT
 		* is like Posts.
 		*/
 		'hierarchical'        => false,
 		'public'              => true,
 		'show_ui'             => true,
 		'show_in_menu'        => true,
 		'show_in_nav_menus'   => true,
 		'show_in_admin_bar'   => true,
 		'menu_position'       => 1,
 		'can_export'          => true,
 		'has_archive'         => true,
 		'exclude_from_search' => false,
 		'publicly_queryable'  => true,
 		'capability_type'     => 'page',
 	);

 	// Registering your Custom Post Type
 	register_post_type( 'servicos', $args );


  // Set UI labels for Custom Post Type
 	$labels = array(
 		'name'                => _x( 'Parceiros', 'Post Type General Name', 'twentythirteen' ),
 		'singular_name'       => _x( 'Parceiro', 'Post Type Singular Name', 'twentythirteen' ),
 		'menu_name'           => __( 'Parceiros', 'twentythirteen' ),
 		'parent_item_colon'   => __( 'Parent Parceiro', 'twentythirteen' ),
 		'all_items'           => __( 'Todos os Parceiros', 'twentythirteen' ),
 		'view_item'           => __( 'View Parceiro', 'twentythirteen' ),
 		'add_new_item'        => __( 'Adicionar novo Parceiro', 'twentythirteen' ),
 		'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
 		'edit_item'           => __( 'Editar Parceiro', 'twentythirteen' ),
 		'update_item'         => __( 'Update Parceiro', 'twentythirteen' ),
 		'search_items'        => __( 'Procurar Parceiro', 'twentythirteen' ),
 		'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
 		'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
 	);

 // Set other options for Custom Post Type

 	$args = array(
 		'label'               => __( 'parceiros', 'twentythirteen' ),
 		'description'         => __( 'Movie news and reviews', 'twentythirteen' ),
 		'labels'              => $labels,
 		// Features this CPT supports in Post Editor
 		'supports'            => array( 'title', 'editor', 'thumbnail' ),
 		// You can associate this CPT with a taxonomy or custom taxonomy.
 	// 	'taxonomies'          => array( 'genres' ),
 		/* A hierarchical CPT is like Pages and can have
 		* Parent and child items. A non-hierarchical CPT
 		* is like Posts.
 		*/
 		'hierarchical'        => false,
 		'public'              => true,
 		'show_ui'             => true,
 		'show_in_menu'        => true,
 		'show_in_nav_menus'   => true,
 		'show_in_admin_bar'   => true,
 		'menu_position'       => 1,
 		'can_export'          => true,
 		'has_archive'         => true,
 		'exclude_from_search' => false,
 		'publicly_queryable'  => true,
 		'capability_type'     => 'page',
 	);

 	// Registering your Custom Post Type
 	register_post_type( 'parceiros', $args );



 	  // Set UI labels for Custom Post Type
 	$labels = array(
 		'name'                => _x( 'Portfólios', 'Post Type General Name', 'twentythirteen' ),
 		'singular_name'       => _x( 'Portfólio', 'Post Type Singular Name', 'twentythirteen' ),
 		'menu_name'           => __( 'Portfólios', 'twentythirteen' ),
 		'parent_item_colon'   => __( 'Parent Portfólio', 'twentythirteen' ),
 		'all_items'           => __( 'Todos os Portfólios', 'twentythirteen' ),
 		'view_item'           => __( 'View Portfólio', 'twentythirteen' ),
 		'add_new_item'        => __( 'Adicionar novo Portfólio', 'twentythirteen' ),
 		'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
 		'edit_item'           => __( 'Editar Portfólio', 'twentythirteen' ),
 		'update_item'         => __( 'Update Portfólio', 'twentythirteen' ),
 		'search_items'        => __( 'Procurar Portfólio', 'twentythirteen' ),
 		'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
 		'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
 	);

 // Set other options for Custom Post Type

 	$args = array(
 		'label'               => __( 'portfolio', 'twentythirteen' ),
 		'description'         => __( 'Movie news and reviews', 'twentythirteen' ),
 		'labels'              => $labels,
 		// Features this CPT supports in Post Editor
 		'supports'            => array( 'title', 'editor', 'thumbnail' ),
 		// You can associate this CPT with a taxonomy or custom taxonomy.
 	// 	'taxonomies'          => array( 'genres' ),
 		/* A hierarchical CPT is like Pages and can have
 		* Parent and child items. A non-hierarchical CPT
 		* is like Posts.
 		*/
 		'hierarchical'        => false,
 		'public'              => true,
 		'show_ui'             => true,
 		'show_in_menu'        => true,
 		'show_in_nav_menus'   => true,
 		'show_in_admin_bar'   => true,
 		'menu_position'       => 1,
 		'can_export'          => true,
 		'has_archive'         => true,
 		'exclude_from_search' => false,
 		'publicly_queryable'  => true,
 		'capability_type'     => 'page',
 	);

 	// Registering your Custom Post Type
 	register_post_type( 'portfolio', $args );


 	  // Set UI labels for Custom Post Type
 	$labels = array(
 		'name'                => _x( 'As Sócias', 'Post Type General Name', 'twentythirteen' ),
 		'singular_name'       => _x( 'as-socias', 'Post Type Singular Name', 'twentythirteen' ),
 		'menu_name'           => __( 'As Sócias', 'twentythirteen' ),
 		'parent_item_colon'   => __( 'Parent as-socias', 'twentythirteen' ),
 		'all_items'           => __( 'Todos as-socias', 'twentythirteen' ),
 		'view_item'           => __( 'View as-socias', 'twentythirteen' ),
 		'add_new_item'        => __( 'Adicionar novo as-socias', 'twentythirteen' ),
 		'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
 		'edit_item'           => __( 'Editar as-socias', 'twentythirteen' ),
 		'update_item'         => __( 'Update as-socias', 'twentythirteen' ),
 		'search_items'        => __( 'Procurar as-socias', 'twentythirteen' ),
 		'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
 		'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
 	);

 // Set other options for Custom Post Type

 	$args = array(
 		'label'               => __( 'as-socias', 'twentythirteen' ),
 		'description'         => __( 'Movie news and reviews', 'twentythirteen' ),
 		'labels'              => $labels,
 		// Features this CPT supports in Post Editor
 		'supports'            => array( 'title', 'editor', 'thumbnail' ),
 		// You can associate this CPT with a taxonomy or custom taxonomy.
 	// 	'taxonomies'          => array( 'genres' ),
 		/* A hierarchical CPT is like Pages and can have
 		* Parent and child items. A non-hierarchical CPT
 		* is like Posts.
 		*/
 		'hierarchical'        => false,
 		'public'              => true,
 		'show_ui'             => true,
 		'show_in_menu'        => true,
 		'show_in_nav_menus'   => true,
 		'show_in_admin_bar'   => true,
 		'menu_position'       => 1,
 		'can_export'          => true,
 		'has_archive'         => true,
 		'exclude_from_search' => false,
 		'publicly_queryable'  => true,
 		'capability_type'     => 'page',
 	);

 	// Registering your Custom Post Type
 	register_post_type( 'as-socias', $args );


 }

 add_action( 'init', 'custom_post_type', 0 ); 


//======================== Custom post type para serviços ========================//


//=================== Custom_post_type com o plugin CMB2 =========================//

add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_sample_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_yourprefix_';

    /**
     * Initiate the metabox
     */
    // Serviços
    $cmb = new_cmb2_box( array(
        'id'            => 'Servicos',
        'title'         => __( 'Sub-conteúdos', 'cmb2' ),
        'object_types'  => array( 'servicos', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );


   $cmb->add_field( array(
	    'name'    => 'Completa',
	    'desc'    => 'Adicionar o texto na sessão "Completa" a página',
	    'default' => '',
	    'id'      => 'wiki_test_textarea_completa',
	    'type'    => 'textarea',
	) ); 

   $cmb->add_field( array(
	    'name'    => 'Parcial',
	    'desc'    => 'Adicionar o texto na sessão "Parcial" a página',
	    'default' => '',
	    'id' 	  => 'wiki_test_textarea_parcial',
	    'type'    => 'textarea',
	) ); 

   $cmb->add_field( array(
	    'name'    => 'Do Dia',
	    'desc'    => 'Adicionar o texto na sessão "Do Dia" a página',
	    'default' => '',
	    'id'      => 'wiki_test_textarea_dodia',
	    'type'    => 'textarea',
	) ); 

	
	// Parceiros
    $cmb = new_cmb2_box( array(
        'id'            => 'Parceiros',
        'title'         => __( 'Sub-conteúdos', 'cmb2' ),
        'object_types'  => array( 'parceiros', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );   

	$cmb->add_field( array(
	    'name'    => 'Descrição do parceiro',
	    'desc'    => 'Descrição que ficará abaixo do nome do parceiro',
	    'default' => '',
	    'id'      => 'wiki_text_descricao_parceiro',
	    'type'    => 'text',
	) );

	$cmb->add_field( array(
	    'name'    => 'Mensagem da máscara',
	    'desc'    => 'Mensagem que será apresentada na máscara',
	    'default' => '',
	    'id'      => 'wiki_text_mensagem_mascara',
	    'type'    => 'textarea',
	) );


	// Portfólio
    $cmb = new_cmb2_box( array(
        'id'            => 'portfolio',
        'title'         => __( 'Sub-conteúdos', 'cmb2' ),
        'object_types'  => array( 'portfolio', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );   

	$cmb->add_field( array(
	    'name'    => 'Local',
	    'desc'    => 'Nome do local onde foi realizado o evento',
	    'default' => '',
	    'id'      => 'wiki_text_local_portfolio',
	    'type'    => 'text',
	) );

	$cmb->add_field( array(
        'name' => 'Lista de imagens',
        'desc' => 'Adicionar as imagens',
        'id'   => 'wiki_test_imagens_portfolio',
        'type' => 'file_list',
        'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
            // 'query_args' => array( 'type' => 'image' ), // Only images attachment
        // Optional, override default text strings
        'text' => array(
            'add_upload_files_text' => 'Adicionar', // default: "Add or Upload Files"
            'remove_image_text' => 'Remover', // default: "Remove Image"
            'file_text' => 'Replacement', // default: "File:"
            'file_download_text' => 'Replacement', // default: "Download"
            'remove_text' => 'Replacement', // default: "Remove"
        ),
    ) );

		$cmb->add_field( array(
	    'name'    => 'O que foi feito:',
	    'desc'    => 'Descrição do que foi feito no evento',
	    'default' => '',
	    'id'      => 'wiki_text_descricao_evento',
	    'type'    => 'textarea',
	) );    


	// As Sócias
    $cmb = new_cmb2_box( array(
        'id'            => 'as-socias',
        'title'         => __( 'Sub-conteúdos', 'cmb2' ),
        'object_types'  => array( 'as-socias', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );   

	$cmb->add_field( array(
        'name' => 'Lista de imagens',
        'desc' => 'Adicionar as imagens',
        'id'   => 'wiki_test_imagens_associas',
        'type' => 'file_list',
        'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
            // 'query_args' => array( 'type' => 'image' ), // Only images attachment
        // Optional, override default text strings
        'text' => array(
            'add_upload_files_text' => 'Adicionar', // default: "Add or Upload Files"
            'remove_image_text' => 'Remover', // default: "Remove Image"
            'file_text' => 'Replacement', // default: "File:"
            'file_download_text' => 'Replacement', // default: "Download"
            'remove_text' => 'Replacement', // default: "Remove"
        ),
    ) );    


}


//=================== Custom_post_type com o plugin CMB2 =========================// 

 add_theme_support( 'post-thumbnails' );


// offset the main query on the home page
function tutsplus_offset_main_query ( $query ) {
     if ( $query->is_home() && $query->is_main_query() ) {
         $query->set( 'offset', '1' );
    }
 }


 //  O número 80 é a quantidade de caracteres a exibir.
function the_excerpt_max_charlength($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;

		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo $subex;
			}
			echo '...';
		} else {
			echo $excerpt;
		}
	} 


function granostudio_scripts_child() {

 //Desabilitar jquery
 wp_deregister_script( 'jquery' );

 // Theme stylesheet.
 wp_enqueue_style( 'granostudio-style', get_stylesheet_uri()  );
 // import fonts (Google Fonts)
 wp_enqueue_style('granostudio-style-fonts', get_stylesheet_directory_uri() . '/css/fonts/fonts.css');
 // Theme front-end stylesheet
 wp_enqueue_style('granostudio-style-front', get_stylesheet_directory_uri() . '/css/main.min.css');

 wp_enqueue_style('carousel-min', get_stylesheet_directory_uri() . '/css/owl.carousel.css'); 

 wp_enqueue_style('carousel-theme', get_stylesheet_directory_uri() . '/css/owl.theme.default.css');  

 wp_enqueue_script('carousel-js', get_stylesheet_directory_uri() . '/js/src/owl.carousel.js', '2.3.4', array('jquery'), true);

 // scripts js
 wp_enqueue_script('granostudio-scripts', get_stylesheet_directory_uri() . '/js/dist/scripts.min.js', '000001', false, true);
 // network effet home

}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts_child' );
