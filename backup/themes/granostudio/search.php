<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="<?php echo is_active_sidebar( 'sidebar_blog' ) ? 'col-lg-8' : 'col-lg-8 col-sm-offset-2'; ?>">

                <h1 class="page-header">
                    <?php printf( __( '<span class="glyphicon glyphicon-search" aria-hidden="true"></span> %s', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?>
                </h1>

                <!-- First Blog Post -->
                <?php
                if( have_posts() ) {
                  while ( have_posts() ) {
                    the_post();
                    echo '<div class="row">';
                    get_template_part( 'template-parts/content-post', 'content-post' );
                    echo '</div>';
                  }
                } else {
                  /* No posts found */
                }
                 ?>

                <!-- Pager -->
                <ul class="pager">

                    <li class="previous"><?php next_posts_link( 'Older posts' ); ?></li>
                    <li class="next"><?php previous_posts_link( 'Newer posts' ); ?></li>

                </ul>
            </div>



            <!-- Blog Sidebar Widgets Column -->
            <?php get_sidebar(); ?>

        </div>
        <!-- /.row -->

        <hr>


    </div>
    <!-- /.container -->

<?php get_footer(); ?>
