<?php get_header(); ?>

<div class="container">

  <div class="row" style="margin-top: 150px;margin-bottom: 80px;">
    <div class="col-sm-6 col-sm-offset-3">
      <h1>Blog e Imprensa</h1>
      <hr class="titulo">
    </div>
  </div>

  <div class="banner">
  
  <div class="col-sm-8">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <!-- <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol> -->

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <?php
      // start by setting up the query
      $query = new WP_Query( array(
          'post_type' => 'banner-blog',
          'posts_per_page' => 1,
      ));

      // now check if the query has posts and if so, output their content in a banner-box div
      if ( $query->have_posts() ) { ?>

              <?php while ( $query->have_posts() ) : $query->the_post();
              $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                <div class="item active">

                  <div class="descricao-banner">
                    <ul class="lista-categoria">
                      <?php
                      foreach((get_the_category()) as $category) {
                        echo '<li class="hashtags">' . $category->cat_name . '</li>';
                      }
                      ?>
                    </ul>
                    <a href="<?php echo esc_url( $url ); ?>"><h2><?php the_title() ?></h2></a>
                    <p style="font-size:15px;"><?php echo the_excerpt_max_charlength(150); ?></p>
                  </div>
                  <div class="img-banner">
                    <?php the_post_thumbnail( ); ?>
                  </div>
                  <!-- <a href="<?php echo esc_url( $url ); ?>"><div class="botao"><p>Leia Mais</p></div></a> -->

                <div class="slide-progress"></div>
                </div>
              <?php endwhile; ?>

      <?php }
      wp_reset_postdata();
      ?>

        <!--  Segundo Loop -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'banner-blog',
            'offset' => 1,
        ));

        // now check if the query has posts and if so, output their content in a banner-box div
        if ( $query->have_posts() ) { ?>

                <?php while ( $query->have_posts() ) : $query->the_post();
                    $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                  <div class="item">

                    <div class="descricao-banner">
                      <ul class="lista-categoria">
                        <?php
                        foreach((get_the_category()) as $category) {
                          echo '<li class="hashtags">' . $category->cat_name . '</li>';
                        }
                        ?>
                      </ul>
                      <a href="<?php echo esc_url( $url ); ?>"><h2><?php the_title() ?></h2></a>+
                      <p style="font-size:15px;"><?php echo the_excerpt_max_charlength(150); ?></p>
                    </div>
                    <div class="img-banner">
                      <?php the_post_thumbnail( ); ?>
                    </div>
                    <!-- <a href="<?php echo esc_url( $url ); ?>"><div class="botao"><p>Leia Mais</p></div></a> -->

                  <div class="slide-progress"></div>
                  </div>
                <?php endwhile; ?>

        <?php }
        wp_reset_postdata();
        ?>

    </div>


        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="background-image:none">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next" style="background-image:none">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>

  </div>

  <div class="row posts">

    <?php
     $args = array( 'post_type' => 'post', 'posts_per_page' => 10);
     $loop = new WP_Query( $args );

     if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

       <div class="post col-sm-4">
         <!-- <a href="<?php echo get_the_permalink(); ?>"> -->
            <?php the_post_thumbnail( ); ?>
          <!-- </a> -->
            <div class="excerpt">
              <ul class="lista-categoria">
                <?php
                foreach((get_the_category()) as $category) {
                  echo '<li class="hashtags">' . $category->cat_name . '</li>';
                }
                ?>
              </ul>
              <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
              <p><?php the_excerpt(); ?></p>
            </div>

          <div class="mask-post col-sm-4">
            <a href="<?php echo get_the_permalink(); ?>">
            <div class="text">
              <ul class="lista-categoria">
                <?php
                foreach((get_the_category()) as $category) {
                  echo '<li class="hashtags">' . $category->cat_name . '</li>';
                }
                ?>
              </ul>
              <h3><?php echo get_the_title(); ?></h3>
              <button type="button" class="botao" name="button">Leia Mais</button>
            </div>
          </div>
       </div>

   <?php endwhile; // end of the loop. ?>
   <?php endif; ?>

  </div>

</div>

<?php get_footer(); ?>
