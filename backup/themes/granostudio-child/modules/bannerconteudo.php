<?php

/**
* Módulo:
* ***** Banner com conteúdo - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_bannerconteudo($bannerconteudo, $key){
    $post_meta = get_post_meta($bannerconteudo, 'banner_slides');
    ?>

    <?php
    echo '<div id="grano-carousel-conteudo" class="grano-carousel-conteudo-'.$key.' owl-theme">';
    foreach ( (array) $post_meta[0] as $attachment_id => $value ) {
        $img_url = wp_get_attachment_image_src( $value['banner_imagem_id'], 'large');
        $banner_titulo = array_key_exists('banner_titulo',$post_meta[0][0]) ? $value['banner_titulo'] : '';
        $banner_descricao = array_key_exists('banner_descricao',$post_meta[0][0]) ? $value['banner_descricao'] : '';
        $link_id = array_key_exists('banner_link',$post_meta[0][0]) ? $value['banner_link'] : '';
        $link_url = $link_code = $link_code_final = '';

        if (!empty($link_id)) {
          $link_url = get_permalink($link_id);
          $link_code = ' <a href="'.$link_url.'">';
          $link_code_final = '</a>';
        }

        echo '<div class="item" style="background-image:url('.$img_url[0].')">';
        echo !empty($banner_titulo)?'<h1 class="title">'.$link_code.$banner_titulo.$link_code_final.'</h1>':'';
        echo !empty($banner_descricao)?'<div class="conteudo">'.$link_code.$banner_descricao.$link_code_final.'</div>':'';
        echo '<div class="hover">'.$link_code.$link_code_final.'</div>';
        echo '<a href="http://sistran.tempsite.ws/index.php/sobre/"><button type="button" class="botao botao-banner" name="button">Conheça a Sistran Brasil</button></a>';
        echo '</div>';
    }
    echo '</div>';
}
 ?>
