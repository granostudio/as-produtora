<?php get_header(); ?>

<!-- Div banner -->
<div class="banner-parcerias">
  <div class="col-sm-8 col-sm-offset-2">
    <h1>A Sistran Brasil se une às princiais empresas globais</h1>
    <p>de tecnologia da informação para entregar o que há de mais inovador às Seguradoras. Conheça os parceiros de primeira classe que compõem nosso portfólio de produtos e soluções de negócio:</p>
  </div>
</div>
<!-- Fim Div Banner -->

<!-- Div Conteúdo Parceiros -->
<div class="parceiros">
  <div class="container">
    <div class="col-sm-10 col-sm-offset-1">
      <div class="row">
        <div class="col-img">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice1@2x.png">
        </div>
        <div class="col-right">
          <h2>Plataforma tecnológica de transformação digital presente nas maiores Seguradoras do mundo.</h2>
          <p>Abordagem completa para desenhar e ativar aplicações ominichannel em tempo recorde, com foco em Customer Services, Marketing, Sales dentre outras.</p>
        </div>
      </div>

      <div class="row">
        <div class="col-img">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice2@2x.png">
        </div>
        <div class="col-right">
          <h2>Soluções de inteligência analítica para identificar fatores críticos, acompanhamento da execução de metas,</h2>
          <p>simulações, executive mining, dashboard mining e cockpit mining Itegrável a midias sociais (Facebook, Instagram, Twitter, YouTube, LinkedIn, Reclame Aqui)</p>
        </div>
      </div>

      <div class="row">
        <div class="col-img">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice3@2x.png">
        </div>
        <div class="col-right">
          <h2>A Sistran Brasil é Microsoft Gold Certified Partner e trabalha com a fabricante para prover soluções</h2>
          <p>utilizando softwares de última geração</p>
        </div>
      </div>

      <div class="row">
        <button type="button" class="botao botao-home" name="button">Veja também: Portfólio da Sistran Brasil</button>
      </div>

    </div>
  </div> <!-- Fim Container -->
</div> <!-- Fim Div Parceiros -->
<!-- Fim Div Conteúdo Parceiros -->

<?php get_footer(); ?>
