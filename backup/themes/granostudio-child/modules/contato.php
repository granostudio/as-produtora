<?php

/**
* Módulo:
* ***** Contato - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_contato($contato,$key){
  ?>
  <div class="bg-contato">
    <div class="mask"></div>
    <div id="contato" class="container contato-<?php echo $key; ?>">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <!-- <h1>Fale Conosco</h1> -->
            <?php echo do_shortcode('[contact-form-7 id="'.$contato.'"]'); ?>
        </div>
      </div>
    </div>
  </div>

  <?php
}
 ?>
