<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- btn voltar -->
<!--   <div class="btnvoltar-single">
    <a href="<?php echo get_home_url(); ?>" class="btn"><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
  </div> -->
<!-- /btn voltar -->

<!-- Page Content --> 
    <div class="blog-single">


            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                <?php
                 // post thumbanil 
                 if (has_post_thumbnail()) {
                    the_post_thumbnail('large', array( 'class' => 'img-responsive' ));
                }
            ?>

      <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="<?php echo is_active_sidebar( 'sidebar_blog' ) ? 'col-sm-10' : 'col-sm-10 col-sm-offset-1'; ?>">

                <!-- Blog Post -->

        		
                  <div ng-view></div>
                <!-- Title -->
                <h1><?php echo get_the_title(); ?></h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php echo get_the_author(); ?></a>
                </p>


                <!-- Date/Time -->
				<?php $data_atualização = get_the_modified_date(); ?>
                <p><span class="fa fa-clock-o"></span> <?php echo get_the_date(); ?> | <?php echo !empty($data_atualização) ? "Atualizado dia ".$data_atualização : ''; ?></p>

                <hr>


                <!-- Post Content -->
                <?php the_content(); ?>

                <hr>

                <!-- Blog Comments -->

                <div class="fb-comments" data-href="http://sistran.tempsite.ws/" data-numposts="5" style="width: 100%;"></div>    

            </div>
					<?php endwhile; // end of the loop. ?>

            <!-- Blog Sidebar Widgets Column -->
<!--             <?php get_sidebar(); ?> -->


          </div>
        <!-- /.row -->

        </div>

    </div>
    <!-- /.container -->

<?php get_footer(); ?>
