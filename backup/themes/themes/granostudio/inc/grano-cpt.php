<?php

// checar se o post type está habilitado

$custom_post_type = get_option('custom_post_type');

$modelosCheck = new modelosCheck();
$ativos = $modelosCheck->ativos();

// destativar post type post por default
add_action( 'admin_menu', 'remove_admin_menus' );
add_action( 'wp_before_admin_bar_render', 'remove_toolbar_menus' );
// add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );

function remove_admin_menus() {
    remove_menu_page( 'edit.php' );
}

function remove_toolbar_menus() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'new-post' );
}

// function remove_dashboard_widgets() {
//     global $wp_meta_boxes;
//     unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
//     unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
// }

if( !empty($ativos)){
  foreach ($ativos as $key => $value) {

    switch ($value) {
      case 'post':
            add_action( 'admin_menu', 'add_admin_menus' );
            add_action( 'wp_before_admin_bar_render', 'add_toolbar_menus', 999 );

            function add_admin_menus() {
                  add_menu_page(
                  __( 'Posts', 'granostudio' ),
                  'Posts',
                  'Post',
                  'edit.php',
                  '',
                  'dashicons-admin-post',
                  1 );
            }

            function add_toolbar_menus() {
              global $wp_admin_bar;
              $args = array(
                'id'     => 'new-post',     // id of the existing child node (New > Post)
                'title'  => 'Post', // alter the title of existing node
                'parent' => 'new-content',
                'href'   =>  'post-new.php',
              );
              $wp_admin_bar->add_node( $args );
            }

            // metabox blog/post
            add_action( 'cmb2_init', 'cmb2_post' );
            /**
             * Define the metabox and field configurations.
             */
            function cmb2_post() {

                // Start with an underscore to hide fields from custom fields list
                $prefix = 'post_';

                /**
                 * Initiate the metabox
                 */
                $cmb = new_cmb2_box( array(
                    'id'            => 'post_SEO',
                    'title'         => __( 'SEO', 'cmb2' ),
                    'object_types'  => array( 'post', ), // Post type
                    'context'       => 'side',
                    'priority'      => 'low',
                    // 'show_names'    => true, // Show field names on the left
                    // 'cmb_styles' => false, // false to disable the CMB stylesheet
                    // 'closed'     => true, // Keep the metabox closed by default
                ) );

                $cmb->add_field( array(
                    'name'    => 'Facebook Image',
                    'desc'    => 'Formato 1200px x 630px',
                    'id'      => $prefix.'_seo_faceimage',
                    'type'    => 'file',
                    // Optionally hide the text input for the url:
                    'options' => array(
                        'url' => false,
                    ),
                ) );

            }

        break;

      case 'clientes':

        // CLIENTES
        function cpt_clientes() {

          /**
           * Register a custom post type
           *
           * Supplied is a "reasonable" list of defaults
           * @see register_post_type for full list of options for register_post_type
           * @see add_post_type_support for full descriptions of 'supports' options
           * @see get_post_type_capabilities for full list of available fine grained capabilities that are supported
           */
          register_post_type('clientes', array(
            'labels' => array(
              'name'                  => _x( 'Clientes', 'Post type general name', 'granostudio' ),
              'singular_name'         => _x( 'Cliente', 'Post type singular name', 'granostudio' ),
              'menu_name'             => _x( 'Clientes', 'Admin Menu text', 'granostudio' ),
              'name_admin_bar'        => _x( 'Clientes', 'Add New on Toolbar', 'granostudio' ),
              'add_new_item'          => __( 'Add New Cliente', 'textdomain' ),
            ),
            'description' => '',
            'public' => true,
            'exclude_from_search' => null,
            'publicly_queryable' => null,
            'show_ui' => true,
            'show_in_nav_menus' => null,
            'hierarchical' => false,
            'supports' => array(
              'title',
              'editor',
              'thumbnail'
            ),
            'capability_type' => 'post',
            'menu_position' => 2,
            'menu_icon' => 'dashicons-id',
            'register_meta_box_cb' => 'logoCliente',
          ));
        }
        add_action('init', 'cpt_clientes');

        // mudar nome do metabox para logo do cliente
        function logoCliente(){
          remove_meta_box( 'postimagediv', 'clientes', 'side' );
          add_meta_box('postimagediv', __('Logo do Cliente'), 'post_thumbnail_meta_box', 'clientes', 'side', 'high');
        }

        // add_action( 'admin_menu', 'logoCliente' );
        break;

      case 'nucleos':

            // PORTFOLIO
            function cpt_nucleo() {

              /**
               * Register a custom post type
               *
               * Supplied is a "reasonable" list of defaults
               * @see register_post_type for full list of options for register_post_type
               * @see add_post_type_support for full descriptions of 'supports' options
               * @see get_post_type_capabilities for full list of available fine grained capabilities that are supported
               */
              register_post_type('nucleos', array(
                'labels' => array(
                  'name'                  => _x( 'Núcleos', 'Post type general name', 'granostudio' ),
                  'singular_name'         => _x( 'nucleo', 'Post type singular name', 'granostudio' ),
                  'menu_name'             => _x( 'Núcleos', 'Admin Menu text', 'granostudio' ),
                  'name_admin_bar'        => _x( 'Núcleos', 'Add New on Toolbar', 'granostudio' ),
                  'add_new_item'          => __( 'Add New Nucleo', 'textdomain' ),
                ),
                'description' => '',
                'public' => true,
                'exclude_from_search' => null,
                'publicly_queryable' => null,
                'show_ui' => true,
                'show_in_nav_menus' => null,
                'hierarchical' => false,
                'supports' => array(
                  'title',
                  'editor',
                  'thumbnail'
                ),
                'capability_type' => 'post',
                'menu_position' => 1,
                'menu_icon' => 'dashicons-format-gallery',
                'taxonomies' => array( 'category' ),
                // 'register_meta_box_cb' => 'add_metaboxes',
              ));
            }
            add_action('init', 'cpt_nucleo');

            // galeria de imagens para portfolio
            add_action( 'cmb2_init', 'cmb2_nucleo' );
            /**
             * Define the metabox and field configurations.
             */
            function cmb2_nucleo() {

                // Start with an underscore to hide fields from custom fields list
                $prefix = 'nucleo_';

                /**
                 * Initiate the metabox
                 */
                $cmb = new_cmb2_box( array(
                    'id'            => 'nucleo',
                    'title'         => __( 'Links', 'cmb2' ),
                    'object_types'  => array( 'nucleos', ), // Post type
                    'context'       => 'normal',
                    'priority'      => 'high',
                    // 'show_names'    => true, // Show field names on the left
                    // 'cmb_styles' => false, // false to disable the CMB stylesheet
                    // 'closed'     => true, // Keep the metabox closed by default
                ) );

                $cmb->add_field( array(
                  'name'    => 'Tipo de link',
                  'id'      => $prefix.'tipo',
                  'type'    => 'radio_inline',
                  'options' => array(
                      'Interno' => __( 'Interno', 'cmb' ),
                      'Externo'   => __( 'Externo', 'cmb' ),
                  ),
                ) );

                // Add new field
                $cmb->add_field( array(
                    'name'        => __( 'Link Interno' ),
                    'id'          => $prefix.'post',
                    'type'        => 'post_search_text', // This field type
                    // post type also as array
                    'post_type'   => 'post',
                    // Default is 'checkbox', used in the modal view to select the post type
                    'select_type' => 'radio',
                    // Will replace any selection with selection from modal. Default is 'add'
                    'select_behavior' => 'replace',
                ) );

                $cmb->add_field( array(
                    'name' => __( 'Link Externo', 'cmb' ),
                    'id'   => $prefix.'link',
                    'type' => 'text_url',
                    // 'protocols' => array( 'http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet' ), // Array of allowed protocols
                ) );

            }


        break;

        case 'banner':

              // Banner com coteúdo
              function cpt_banner() {

                /**
                 * Register a custom post type
                 *
                 * Supplied is a "reasonable" list of defaults
                 * @see register_post_type for full list of options for register_post_type
                 * @see add_post_type_support for full descriptions of 'supports' options
                 * @see get_post_type_capabilities for full list of available fine grained capabilities that are supported
                 */
                register_post_type('banner', array(
                  'labels' => array(
                    'name'                  => _x( 'Banner', 'Post type general name', 'granostudio' ),
                    'singular_name'         => _x( 'Banner', 'Post type singular name', 'granostudio' ),
                    'menu_name'             => _x( 'Banner', 'Admin Menu text', 'granostudio' ),
                    'name_admin_bar'        => _x( 'Banner', 'Add New on Toolbar', 'granostudio' ),
                    'add_new_item'          => __( 'Add New Banner', 'textdomain' ),
                  ),
                  'description' => '',
                  'public' => true,
                  'exclude_from_search' => null,
                  'publicly_queryable' => null,
                  'show_ui' => true,
                  'show_in_nav_menus' => null,
                  'hierarchical' => false,
                  'supports' => array(
                    'title',
                  ),
                  'capability_type' => 'post',
                  'menu_position' => 1,
                  'menu_icon' => 'dashicons-images-alt2',
                  // 'register_meta_box_cb' => 'add_metaboxes',
                ));
              }
              add_action('init', 'cpt_banner');

              // galeria de imagens para portfolio
              add_action( 'cmb2_init', 'cmb2_banner' );
              /**
               * Define the metabox and field configurations.
               */
              function cmb2_banner() {

                  // Start with an underscore to hide fields from custom fields list
                  $prefix = 'banner_';

                  /**
                   * Initiate the metabox
                   */
                  $cmb = new_cmb2_box( array(
                      'id'            => $prefix.'banner',
                      'title'         => __( 'Slides', 'cmb2' ),
                      'object_types'  => array( 'banner', ), // Post type
                      'context'       => 'normal',
                      'priority'      => 'high',
                      // 'show_names'    => true, // Show field names on the left
                      // 'cmb_styles' => false, // false to disable the CMB stylesheet
                      // 'closed'     => true, // Keep the metabox closed by default
                  ) );

                  $group_slides = $cmb->add_field( array(
                      'id'          => $prefix.'slides',
                      'type'        => 'group',
                      // 'description' => __( 'Generates reusable form entries', 'cmb2' ),
                      // 'repeatable'  => false, // use false if you want non-repeatable group
                      'options'     => array(
                          'group_title'   => __( 'Slide {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
                          'add_button'    => __( 'Add Slide', 'cmb2' ),
                          'remove_button' => __( 'Remove Slide', 'cmb2' ),
                          'sortable'      => true, // beta
                          // 'closed'     => true, // true to have the groups closed by default
                      ),
                  ) );
                  $cmb->add_group_field( $group_slides, array(
                      'name' => 'Titulo',
                      'id'   => $prefix.'titulo',
                      'type' => 'text',
                      // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
                  ) );
                  $cmb->add_group_field( $group_slides, array(
                      'name' => 'Texto',
                      // 'description' => 'Write a short description for this entry',
                      'id'   => $prefix.'descricao',
                      'type' => 'textarea_small',
                  ) );
                  $cmb->add_group_field( $group_slides, array(
                      'name' => 'Imagem',
                      'id'   => $prefix.'imagem',
                      'type' => 'file',
                      'options' => array(
                          'url' => false, // Hide the text input for the url
                      ),
                  ) );
                  $cmb->add_group_field( $group_slides, array(
                    'name'        => 'Link com conteúdo do site',
                    'id'          => $prefix.'link',
                    'type'        => 'post_search_text', // This field type
                    // post type also as array
                    'post_type'   => array('post','page','clientes'),
                    // Default is 'checkbox', used in the modal view to select the post type
                    'select_type' => 'radio',
                    // Will replace any selection with selection from modal. Default is 'add'
                    'select_behavior' => 'replace',
                  ) );

              }


          break;
    }
  }

}
