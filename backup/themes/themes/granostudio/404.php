<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area container notfound">
			<div class="row">
				<section class="error-404 not-found col-sm-8 col-sm-offset-2 text-center">
						<h1 class="page-title">Nada encontrado para sua pesquisa.</h1>
						<p>O que acha de experimentar outras palavras? :)</p>
				</section><!-- .error-404 -->
			</div>

	</div><!-- .content-area -->

<?php get_footer(); ?>
