<?php

/**
* Módulo:
* ***** Banner com conteúdo - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_bannerconteudo($bannerconteudo, $key){
    $post_meta = get_post_meta($bannerconteudo, 'banner_slides');
    ?>
    <!-- <div class="owl-carousel owl-theme"> -->
      <?php
        // foreach ( (array) $post_meta[0] as $attachment_id => $value ) {
        //   echo '<div class="item">texto</div>';
        // }
       ?>
    <!-- </div> -->

    <div class="container grano-posts">
      <div class="row">
        <div class="col-sm-12 col-md-8 post ">
          <div class="post-border">
            <div class="slide-progress"></div>
    <?php
    echo '<div class="carousel-banner-blog-'.$key.' owl-theme owl-carousel ">';
    foreach ( (array) $post_meta[0] as $attachment_id => $value ) {
        $img_url = wp_get_attachment_image_src( $value['banner_imagem_id'], 'large');
        $banner_titulo = array_key_exists('banner_titulo',$post_meta[0][0]) ? $value['banner_titulo'] : '';
        $banner_descricao = array_key_exists('banner_descricao',$post_meta[0][0]) ? $value['banner_descricao'] : '';
        $link_id = array_key_exists('banner_link',$post_meta[0][0]) ? $value['banner_link'] : '';
        $link_url = $link_code = $link_code_final = '';

        if (!empty($link_id)) {
          $link_url = get_permalink($link_id);
          $link_code = ' <a href="'.$link_url.'">';
          $link_code_final = '</a>';
        }

        echo '<div class="item">';
        echo '<div class="banner-hover">'.$link_code.$link_code_final.'</div>';
        echo "<div class='col1'>";
        echo !empty($banner_titulo)?'<h2 class="title">'.$link_code.$banner_titulo.$link_code_final.'</h2>':'';
        echo !empty($banner_descricao)?'<div class="conteudo">'.$banner_descricao.'</div>':'';
        // echo '<div class="btn btn-primary">Leia Mais</div>';
        echo "</div><div class='col2'>";
        echo '<div class="banner-thumb" style="background-image:url('.$img_url[0].')"></div>';
        echo '</div>';
        echo '</div>';
    }
    echo '</div></div></div>';

}
 ?>
