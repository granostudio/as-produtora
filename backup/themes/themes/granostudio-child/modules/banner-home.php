<?php

/**
* Módulo:
* ***** Externo !Exemplo! - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */
 ?>
<!-- NOME DO SEU MODULO  -->

<div class="header-home">

    <?php
        wp_nav_menu( array(
            'menu'              => 'primary',
            'theme_location'    => 'primary',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'menu',
            'container_id'      => '',
            'menu_class'        => 'nav navbar-nav hidden-xs',
            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            'walker'            => new wp_bootstrap_navwalker())
        );
    ?>
</div>

<div class="banner-home hidden-xs">
  <div class="content-quemsomos">
    <img src="<?php echo get_stylesheet_directory_uri();?>/img/banner-logo.png" alt=""><br/>
    <a href="http://www.gessaude.com.br/quem-somos2/" class="btn btn-primary">Quem Somos</a>
  </div>
    <div class="col-1">
      <div class="bg"></div>
      <div class="mask"></div>
    </div>
    <div class="col-2">
      <div class="linha-1">
        <div class="bg"></div>
        <div class="mask"></div>
        <div class="content">
          <div class="barra-vermelha"></div>
          <h3><a href="#"><span>PRO</span>AMA</a></h3>
          <p><span>
            Programa de <strong>Aceleração</strong><br/>da <strong>Maturidade</strong><br/>de <strong>Gestão da Saúde</strong>
          </span></p>
          <a href="http://www.gessaude.com.br/proama/" class="btn btn-primary">
            Leia Mais
          </a>
        </div>
      </div>
      <div class="linha-2">
        <div class="mask"></div>
        <div class="content">
          <h3><a href="#">Maturidade de<br />Gestão Hospitalar</a></h3>
          <a href="http://www.gessaude.com.br/maturidade-de-gestao-hospitalar/" class="btn btn-primary">
            Leia Mais
          </a>
        </div>
        <canvas></canvas>
      </div>
    </div>
  </div>
</div>
