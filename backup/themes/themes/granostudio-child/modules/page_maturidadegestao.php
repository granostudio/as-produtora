<div class="tituloI titulo">
  <h1>Maturidade de Gestão Hospitalar</h1>
  <!-- <p class="textoT">Modelo de gestão traz ganhos que superam a questão econômica-financeira</p> -->
</div>


<div class="container">
  <div class="referencia">
    <p>"Entidades maduras cumprem metas de forma sistemática, enquanto as imaturas alcançam seus objetivos
      por fruto de esforços individuais, mais ou menos de forma espontânea"</p>
    <b>Paul Harmon</b>
    <p class="autor">autor de "Evaluating an Organization's Business Process Maturity"</p>
  </div>
</div>

<div class="container">
  <p class="texto">A <strong>maturidade da gestão hospitalar</strong> é um conceito já difundido em grandes centros de medicina no exterior e que chega ao Brasil pela <strong>GesSaúde</strong>.<br><br>
Uma administração bem desenvolvida depende da existência de uma visão que envolva todas as áreas internas, com a interligação de cinco pontos:
<strong>tecnologias de gestão, estratégia empresarial, governança corporativa, gerenciamento de processos de negócios e gestão de pessoas.</strong>
Culturalmente, no Brasil  o processo é feito do formato inverso: o foco do gerenciamento de unidades de Saúde é desenvolvido isoladamente,
entre departamentos.</p>
</div>

<div class="container-full editado">
  <div class="coluna1">
    <div class="fundo"></div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-6 texto">
            <p class="colunaT">Quando se fala em qualidade da gestão hospitalar, se pensa de imediato em investimentos em tecnologia. O estudo TIC Saúde, de 2015,
              revelou que 92% das unidades de Saúde do País têm computador e 77% possuem sistema eletrônico para gerenciamento e armazenamento de
              informações dos pacientes. Embora informatizadas, <strong>a maioria dos hospitais brasileiros</strong> utiliza <strong>menos da metade</strong> dos recursos disponíveis
              nas soluções de gestão, ficando em torno de <strong>30% a 40%</strong>, segundo levantamento da GesSaúde.</p>
      </div>
    </div>
  </div>

</div>

<div class="container">
  <p class="texto">A <strong>informatização dos hospitais contribui</strong> de forma significativa para seu melhor funcionamento e atendimento ao paciente,
    <strong>mas não resolve</strong> os problemas da gestão como um todo. "Para se atingir a maturidade de gestão hospitalar,
    é preciso que se consiga extrair ao máximo das ferramentas de gestão. Mas isso só é possível com processos
    bem estruturados que envolvam uma visão global e sistêmica da organização", explica <a href="/quem-somos2/">Roberto Gordilho</a>, fundador da GesSaúde. </p>
</div>

<div class="container">
  <h2>Benefícios de uma gestão madura</h2><br>
  <p class="texto">Pesquisadores holandeses e espanhóis elaboraram um tipo de classificação para avaliar os hospitais da União Europeia
    de acordo com seu nível de maturidade.</p>
    <p> O estudo foi publicado no <strong>British Medical Journal em 2009</strong> concluiu que a
    <strong>maturidade da gestão hospitalar</strong> está associada a:</p>
</div>

<div class="container">
  <div class="rowb">
    <div class="esquerda">
      <div class="row">
        <p class="p1">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/ponto.png" class="ponto">
          menores taxas de complicações hospitalares</p>
      </div>
      <div class="row">
        <p class="p2">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/ponto.png" class="ponto">
          redução de glosas</p>
      </div>
    </div>
      <img src="<?php echo get_stylesheet_directory_uri();?>/img/associacao.png" class="associacao">
    <div class="direita">
      <div class="row">
        <p class="p3">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/ponto.png" class="ponto">
          melhoria na rentabilidade</p>
      </div>
      <div class="row">
        <p class="p4">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/ponto.png" class="ponto">
          aumento da competitividade</p>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <p class="texto">Diante disso, fica claro que apostar no desenvolvimento da maturidade da gestão hospitalar traz ganhos econômico-financeiros,
    além de beneficiar a instituição, funcionários e o principal: cuidar dos pacientes.</p>
</div>

<div class="container">
  <h2 style="text-align: center" >Gestão</h2>
    <div class="comparacao">
      <h3 class="hesquerda">gestão madura</h3>
      <h1>X</h1>
      <h3 class="hdireita">gestão imatura</h3>
    </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-6 gesquerda">
      <h3 class="hesquerda2">gestão madura</h3>
      <div class="divesquerda">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/olho.png" class="img1">
        <p class="titulop">Visão integrada</p>
        <p>Unificação entre pessoas, tecnologia, estratégia, governança e processo</p>
      </div>
      <div class="divesquerda">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/livro2.png"  class="img2">
        <p class="titulop">Qualificação</p>
        <p>Treinamento e capacitação de times para um melhor e mais efetivo uso das tecnologias</p>
      </div>
      <div class="divesquerda">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/teclado.png"  class="img3">
        <p class="titulop">Gestão além da tecnologia</p>
        <p>Ir além do básico oferecido e proposto pelos sistemas de gestão, a partir de uma visão administrativa profissionalizada e com vistas a resultados</p>
      </div>
      <div class="divesquerda">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/grafico.png"  class="img4">
        <p class="titulop">Benefícios financeiros</p>
        <p>Menores taxas de complicações hospitalares, melhoria na rentabilidade e redução das glosas</p>
      </div>
    </div>
    <div class="col-sm-6">
      <h3 class="hesquerda2">gestão imatura</h3>
      <div class="divdireita">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/icone.png"  class="img5">
        <p class="titulop">Gestão Individualizada</p>
        <p>Administração ocorre em silos, por áreas, sem planejamento estratégico integrado, dependendo, portanto, somente de esforços individuais</p>
      </div>
      <div class="divdireita">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/olho2.png"  class="img6">
        <p class="titulop">Falta de visão estratégica</p>
        <p>Mesmo que haja informatização, ferramentas não são utilizadas em toda sua capacidade. Funcionários não são treinados adequadamente para extrair delas indicadores e insights que melhorem e otimizem a gestão e o atendimento ao paciente</p>
      </div>
      <div class="divdireita">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/financa.png"  class="img7">
        <p class="titulop">Finanças obscuras</p>
        <p>A falta de planejamento integrado entre setores impede a otimização de recursos e a capacidade de enxergar gargalos que, uma vez corrigidos, resultam em uma melhora da rentabilidade </p>
      </div>
    </div>
  </div>
</div>

<div class="faixa"></div>

<div class="row4">
  <div class="mask"></div>
  <div class="atencao">
      <h2>ATENÇÃO:</h2>
      <p>A informatização contribui de forma significa para melhoria de gerenciamento e atendimento ao paciente em uma instituição de Saúde, mas não é capaz, sozinha, de promover a maturidade de gestão hospitalar</p>
  </div>
  <canvas></canvas>
</div>

<script src="<?php echo get_stylesheet_directory_uri() . '/js/network-ani.js' ?>" charset="utf-8"></script>
