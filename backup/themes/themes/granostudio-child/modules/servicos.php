<div class="tituloI">
    <h1>Serviços</h1>
</div>

<div class="row1">
    <div class="coluna1">
        <div class="fundo"></div>
    </div>

    <div class="coluna2">
      <div class="consultoria">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/livro.png">
        <h2>CONSULTORIA DE ESTRATÉGIA</h2>
        <p>Contribuir com a formalização dos objetivos traçados, definição do mapa e modelo de gestão, além do acompanhamento alcance destes objetivos</p>
      </div>

      <div class="governanca">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/cabeca.png">
        <h2>GOVERNANÇA CORPORATIVA</h2>
        <p>Otimizar o valor das empresas, a proteção do patrimônio e o equilíbrio dos interesses dos stakeholders de uma organização</p>
      </div>

    </div>

</div>

<div class="row2">
    <div class="coluna3">
      <div class="gerenciamento">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/calendario.png">
        <h2>Gerenciamento de processos</h2>
        <p>Implantar a cultura de gestão por processos e melhoria contínua, realizar o alinhamento
          entre a estratégia da organização e os processos de negócio e aumentar a eficiência da operação</p>
      </div>

      <div class="gerenciamentop">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/agenda.png">
        <h2>Gerenciamento de projetos</h2>
        <p>Implantar a cultura de gerenciamento de projetos, realizar o alinhamento entre a estratégia da organização e os projetos</p>
      </div>
    </div>

    <div class="coluna4">
      <div class="fundo"></div>
    </div>

</div>

<div class="row3">
    <div class="coluna5">
      <div class="fundo"></div>
    </div>

    <div class="coluna6">
      <div class="governanca">
        <h2>GOVERNANÇA DE TI</h2>
        <p>Garantir que as entregas e serviços da TI estejam alinhados a estratégia da organização,
        proporcionar ganhos efetivos de melhoria e eficiência no negócio que gerem aumento de competitividade.</p>
      </div>
    </div>
</div>

<div class="row4">
  <div class="mask"></div>
  <div class="proama">
      <h2>PROGRAMA DE CAPACITAÇÃO E ACELERAÇÃO DE MATURIDADE DA GESTÃO DA SAÚDE (PROAMA)</h2>
      <p>Reúne instituições de sáude (Hospitais, Clínicas, Centro de Diagnósticos, etc.) de pequeno e média
      portes interessados em trabalho com grupos para elevar seus resultados e aumentar sua maturidade de gestão.</p>
  </div>
  <canvas></canvas>
</div>
